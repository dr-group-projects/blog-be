import dotenv from "dotenv";

dotenv.config();

export default {
  port: process.env.PORT,
  mongodb: process.env.MONGODB,
  privateKey: process.env.JWT_KEY,
};
