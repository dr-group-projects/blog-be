import app from "./app";
import config from "./config";
import { dbConnection } from "./database/database";

try {
  dbConnection();
  app.listen(config.port, () => {
    console.log(
      `⚡️[server]: Server is running at https://localhost:${config.port}`
    );
  });
} catch (error) {
  console.log(error);
}
