import bcrypt from "bcrypt";

const saltRounds = 10;

export class Password {
  static async hashPassword(password: string): Promise<string> {
    const hasPassword = await bcrypt.hash(password, saltRounds);
    return hasPassword;
  }

  static async comparePassword(
    plainPassword: string,
    hashedPassword: string
  ): Promise<boolean> {
    const isPasswordValid = await bcrypt.compare(plainPassword, hashedPassword);
    return isPasswordValid;
  }
}
