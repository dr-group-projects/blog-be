import { Body, Post, Route, SuccessResponse, Response, Get } from "tsoa";
import {
  IMessageResponse,
  ILoginRequest,
  ILoginResponse,
  IExists,
} from "../interfaces/req-resp/auth";
import { IUser, UserResponse } from "../interfaces/user.interface";
import { UserModel } from "../models/user.model";
import { Password } from "./password.controller";
import jwt from "jsonwebtoken";
import config from "../config";
import { emailError } from "../errors/auth.error";

@Route("auth")
export class AuthController {
  @Response<IMessageResponse>("200")
  @Response<{ error: object }>("500")
  @SuccessResponse("201")
  @Post("register")
  static async register(@Body() user: IUser): Promise<IMessageResponse> {
    const emailRegex =
      /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (emailRegex.test(user.email) === false) throw new Error(emailError);

    const newUser = await UserModel.create({
      username: user.username,
      password: await Password.hashPassword(user.password),
      email: user.email,
      isValidated: false,
    });

    return {
      message: "user created",
    };
  }

  @Response<IMessageResponse>("401")
  @SuccessResponse("200")
  @Post("login")
  static async login(@Body() user: ILoginRequest): Promise<ILoginResponse> {
    const userData = await UserModel.findOne({ username: user.username });

    const errorMessage = "user or password is incorrect";

    if (!userData) throw new Error(errorMessage);

    const isPasswordValid = Password.comparePassword(
      user.password,
      userData.password
    );

    if (!isPasswordValid) throw new Error(errorMessage);
    const token = jwt.sign(userData.toJSON(), config.privateKey!);

    return {
      username: userData.username,
      token,
    };
  }

  @SuccessResponse("200")
  @Get("valid/email/:email")
  static async validateEmail(email: string): Promise<IExists> {
    const userData = await UserModel.findOne({ email });
    return { exists: userData ? true : false };
  }

  @SuccessResponse("200")
  @Get("valid/username/:username")
  static async validateUsername(username: string): Promise<IExists> {
    const userData = await UserModel.findOne({ username });
    return { exists: userData ? true : false };
  }
}
