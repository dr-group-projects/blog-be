import { Body, Post, Route, SuccessResponse, Response } from "tsoa";
import { IUser, UserResponse } from "../interfaces/user.interface";
import { UserModel } from "../models/user.model";
import { Password } from "./password.controller";

// @Route("user")
// export class UserController {
//   @SuccessResponse("200")
//   @Post()
//   public async createUser(@Body() user: IUser): Promise<UserResponse> {
//     const newUser = await UserModel.create({
//       username: user.username,
//       password: await Password.hashPassword(user.password),
//       email: user.email,
//       isValidated: false,
//     });

//     return {
//       message: "user created",
//       user: newUser,
//     };
//   }
// }
