import cors from "cors";
import express, { Request, Response } from "express";
import morgan from "morgan";
import { Router } from "./routers";
import { swaggerServer, swaggerService } from "./swagger";

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));
app.use(express.static("public"));

app.use("/swagger", swaggerServer, swaggerService);

app.use(Router);

export default app;
