import config from "../config";
import mongoose from "mongoose";

export const dbConnection = async () => {
  try {
    await mongoose.connect(config.mongodb!);
    console.log("⚡️[server]: DB connected");
  } catch (e) {
    console.log("Error while connecting to DB");
  }
};
