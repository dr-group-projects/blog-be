import { IUser } from "../user.interface";

export type ILoginRequest = Omit<IUser, "email">;

export interface ILoginResponse {
  username: string;
  token: string;
}

export interface IMessageResponse {
  message: string;
}

export interface IExists {
  exists: boolean;
}
