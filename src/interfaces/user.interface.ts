import { Validated } from "./validated.interface";

export interface IUser {
  username: string;
  password: string;
  email: string;
}

export interface IUserModel extends IUser, Validated {}

export interface UserResponse {
  message: string;
  user: IUserModel;
}
