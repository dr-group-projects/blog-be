import swaggerUi from "swagger-ui-express";

const DisableTryItOutPlugin = function () {
  return {
    statePlugins: {
      spec: {
        wrapSelectors: {
          allowTryItOutFor: () => () => false,
        },
      },
    },
  };
};

const swaggerServer = swaggerUi.serve;

const swaggerService = swaggerUi.setup(undefined, {
  swaggerOptions: {
    url: "/swagger.json",
    plugins: [DisableTryItOutPlugin],
  },
  customCss: `.swagger-ui .tab li button.tablinks,.swagger-ui section.models,
    .swagger-ui .scheme-container .schemes,
    .swagger-ui .float-right,
    .swagger-ui .info hgroup.main a {display: none}`,
});

export { swaggerServer, swaggerService };
