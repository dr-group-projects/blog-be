import { Router } from "express";
import * as AuthService from "../services/auth.service";

const router = Router();

router.post("/register", AuthService.registerUser);
router.post("/login", AuthService.loginUser);
router.get("/valid/email/:email", AuthService.validateEmail);
router.get("/valid/username/:username", AuthService.validateUsername);

export { router as AuthRouter };
