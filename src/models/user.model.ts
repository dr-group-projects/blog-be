import mongoose, { Schema } from "mongoose";
import { IUserModel } from "../interfaces/user.interface";

const userSchema: Schema = new Schema<IUserModel>({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true, minlength: 8 },
  email: { type: String, required: true, unique: true },
  isValidated: { type: Boolean, required: true },
});

userSchema.set("toJSON", {
  transform: (_, returnedObject: any) => {
    returnedObject.id = returnedObject._id;

    delete returnedObject._id;
    delete returnedObject.__v;
    delete returnedObject.password;
  },
});

const UserModel = mongoose.model<IUserModel>("User", userSchema);

export { UserModel };
