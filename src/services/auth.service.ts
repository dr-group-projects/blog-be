import { Response } from "express";
import { AuthController } from "../controllers/auth.controller";
import { CustomRequest } from "../interfaces/request.interface";
import { IUser } from "../interfaces/user.interface";

const registerUser = async (req: CustomRequest<IUser>, res: Response) => {
  const { email, username, password } = req.body;

  try {
    const newUser = await AuthController.register({
      email,
      username,
      password,
    });

    res.status(200).json({ message: newUser.message });
  } catch (error: any) {
    console.log(error);
    if (error?.keyPattern?.hasOwnProperty("username"))
      return res.status(200).json({ message: "username is being used" });

    if (error?.keyPattern?.hasOwnProperty("email"))
      return res.status(200).json({ message: "email is being used" });

    if (error?.message!)
      return res.status(200).json({ message: error.message });

    res.status(500).json({ error });
  }
};

const loginUser = async (req: CustomRequest<IUser>, res: Response) => {
  const { username, password } = req.body;

  try {
    const user = await AuthController.login({ username, password });
    res.status(200).json(user);
  } catch (err: any) {
    if (err.message)
      res.status(401).json({
        message: err.message,
      });

    res.status(500).json(err);
  }
};

const validateEmail = async (
  req: CustomRequest<{ email: string }>,
  res: Response
) => {
  const { email } = req.params;
  const exists = await AuthController.validateEmail(email);

  res.status(200).json(exists);
};

const validateUsername = async (
  req: CustomRequest<{ username: string }>,
  res: Response
) => {
  const { username } = req.params;
  const exists = await AuthController.validateUsername(username);

  res.status(200).json(exists);
};

export { registerUser, loginUser, validateEmail, validateUsername };
